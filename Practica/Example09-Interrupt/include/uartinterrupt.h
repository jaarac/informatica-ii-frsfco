#ifndef UARTINTERRUPT_H
#define UARTINTERRUPT_H

#include <Arduino.h>

void serialInterruptInit();

void IRAM_ATTR onUARTReceive();

bool isDataReady();

char readSerialData();

#endif