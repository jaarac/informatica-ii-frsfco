#ifndef PININTERRUPT_H
#define PININTERRUPT_H

#include <Arduino.h>

void pinInterruptInit();
void IRAM_ATTR handleInterrupt();
bool isPinOn();

#endif