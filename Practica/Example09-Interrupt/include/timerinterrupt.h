#ifndef TIMERINTERRUPT_H
#define TIMERTINTERRUPT_H

#include <Arduino.h>

void timerInit();
void IRAM_ATTR onTimer();
bool isTimerOn();

#endif