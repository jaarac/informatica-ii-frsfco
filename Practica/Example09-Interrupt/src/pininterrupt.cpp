#include "../include/pininterrupt.h"

volatile bool interrupt_flag = false;
volatile bool buttonPressed = false;
volatile unsigned long lastInterruptTime = 0; // Tiempo de la última interrupción (debounce)

void pinInterruptInit(){
    pinMode(18, INPUT_PULLUP);  // Configura el pin como entrada con resistencia pull-up
    attachInterrupt(digitalPinToInterrupt(18), handleInterrupt, FALLING); // Configura la interrupción en flanco descendente
}

void IRAM_ATTR handleInterrupt() {
  unsigned long currentTime = millis();
    // Verificar si ha pasado el tiempo de debounce (50 ms)
    if (currentTime - lastInterruptTime > 50) {
        interrupt_flag = true;             // Marca que se presionó el botón
        lastInterruptTime = currentTime;  // Actualiza el tiempo de la interrupción
    }
}

bool isPinOn(){
    if (interrupt_flag) {
        interrupt_flag = false;  // Reinicia la bandera
        return 1;
    }
    else return 0;
}
