#include <Arduino.h>
#include "HardwareSerial.h"
#include "../include/uartinterrupt.h"
#include "../include/pininterrupt.h"
#include "../include/timerinterrupt.h"

void setup() {
    serialInterruptInit();
    pinInterruptInit();
    timerInit();
}

void loop() {
    if (isPinOn()) {
        Serial.println("¡Pulsador presionado!");
    }
    if (isTimerOn()) {
        Serial.println("¡Interrupción de temporizador!");
    }
    // Procesar datos recibidos
    if (isDataReady()) {
        Serial.print("Dato recibido por UART: ");
        Serial.println(readSerialData());  // Muestra el dato recibido en el monitor serie
    }
}