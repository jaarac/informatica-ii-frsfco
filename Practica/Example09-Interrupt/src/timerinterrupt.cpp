#include "../include/timerinterrupt.h"

hw_timer_t *timer = NULL;
volatile bool timer_flag = false;

void timerInit(){
// Configuración del temporizador de hardware
    timer = timerBegin(0, 80, true);  // Temporizador 0, prescaler 80 (1 MHz), cuenta ascendente
    timerAttachInterrupt(timer, &onTimer, true);  // Asocia la interrupción
    timerAlarmWrite(timer, 10000000, true);  // Dispara cada 10 segundo (1 MHz / 10000000)
    timerAlarmEnable(timer);  // Habilita la alarma

}

void IRAM_ATTR onTimer() {
    timer_flag = true;  // Activa la bandera al finalizar el temporizador
}

bool isTimerOn(){
    if (timer_flag) {
        timer_flag = false;  // Reinicia la bandera
        return 1;
    }
    else return 0;
}