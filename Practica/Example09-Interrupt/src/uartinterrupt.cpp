#include "../include/uartinterrupt.h"

// Buffer para almacenar datos
volatile char receivedData = '\0';
volatile bool dataReady = false;

void serialInterruptInit(){
    Serial.begin(115200);
    // Configura la interrupción para UART0
    Serial.onReceive(onUARTReceive); // Asocia la ISR al evento de datos recibidos
}

// Función ISR para manejar datos recibidos
void IRAM_ATTR onUARTReceive() {
    while (Serial.available()) {
        receivedData = Serial.read();  // Leer el carácter recibido
        dataReady = true;            // Marca que hay datos listos
    }
}

bool isDataReady(){
    if (dataReady) {
        dataReady = false;  // Reinicia la bandera
        return 1;
    }
    else return 0;
}

char readSerialData(){
    return receivedData;
}