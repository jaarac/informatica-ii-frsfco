#include <stdio.h>
#include <stdint.h>
#include <locale.h>  // Para configurar el idioma y la codificación

// Definición de la estructura sensor_t
typedef struct {
    char name;          // Nombre del sensor (un carácter)
    uint16_t value;     // Valor medido por el sensor
    uint32_t time;      // Tiempo en milisegundos
} sensor_t;

// Puntero para manejar el archivo
FILE *file;

int main() {
    // Inicialización de la estructura de ejemplo
    sensor_t temperature;
    temperature.name = 'T';      // Nombre del sensor: 'T' (Temperatura)
    temperature.value = 30;      // Valor del sensor: 30
    temperature.time = 1500;     // Tiempo de medición: 1500 ms

    // --- Escritura en archivo ---
    // Abrir el archivo en modo escritura ("w" crea el archivo si no existe)
    if ((file = fopen("SENSOR.DAT", "w")) == NULL) {
        printf("\nERROR - No se puede abrir el archivo especificado para escritura\n");
    } else {
        // Escribir los datos de la estructura en el archivo
        fwrite(&temperature, sizeof(sensor_t), 1, file);
        // Cerrar el archivo
        fclose(file);
    }

    // --- Lectura del archivo ---
    // Declarar una estructura para leer los datos desde el archivo
    sensor_t readTemp;

    // Abrir el archivo en modo lectura ("r")
    if ((file = fopen("SENSOR.DAT", "r")) == NULL) {
        printf("\nERROR - No se puede abrir el archivo especificado para lectura\n");
    } else {
        // Mostrar el tamaño de la estructura
        int tam = sizeof(sensor_t);
        printf("Tamanio de la estructura: %d \n", tam);

        // Leer los datos mientras fread devuelva 1 (indica que leyó correctamente)
        while (fread(&readTemp, tam, 1, file) == 1) {
            // Imprimir los datos leídos de la estructura
            printf("Nombre: %c\n", readTemp.name);
            printf("Medicion: %d\n", readTemp.value);
            printf("Tiempo: %lu\n", readTemp.time);
        }

        // Cerrar el archivo después de la lectura
        fclose(file);
    }

    return 0;
}
