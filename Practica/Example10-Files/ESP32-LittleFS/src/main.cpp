#include <Arduino.h>
#include <LittleFS.h>

typedef struct {
  char name[32];
  char pass[32];
}user_t;

void saveCredentials(const char* filename, const user_t credentials[], int numUsers);
void readCredentials(const char* filename, user_t credentials[], int numUsers);
void modifyCredentials(user_t credentials[], int numUsers, const char* newName, const char* newPass, int index);

void setup() {
  Serial.begin(115200);
  
  if (!LittleFS.begin(true)) {
    Serial.println("Error al montar el sistema de archivos");
    while (true);  // Detener ejecución si LittleFS no se monta
  }
  
  // Credenciales a guardar
  user_t credentials[3] = {{"Mario", "123"}, {"Luis", "456"}, {"Diego", "789"}};
  
  // Guardar credenciales en el archivo
  saveCredentials("/datos.dat", credentials, 3);
  
  // Leer credenciales del archivo
  user_t loadedCredentials[3];
  readCredentials("/datos.dat", loadedCredentials, 3);

  // Mostrar credenciales leídas
  for (int i = 0; i < 3; i++) {
    Serial.printf("user: %s, pass: %s\n", loadedCredentials[i].name, loadedCredentials[i].pass);
  }

  // Modificar la credencial en la posición 2 (índice 2)
  modifyCredentials(loadedCredentials, 3, "Pepe", "000", 2);
  
  // Guardar las credenciales modificadas
  saveCredentials("/datos.dat", loadedCredentials, 3);

  readCredentials("/datos.dat", loadedCredentials, 3);

  // Mostrar credenciales leídas
  for (int i = 0; i < 3; i++) {
    Serial.printf("user: %s, pass: %s\n", loadedCredentials[i].name, loadedCredentials[i].pass);
  }
}

void loop() {

}

// Función para guardar las credenciales en el archivo
void saveCredentials(const char* filename, const user_t credentials[], int numUsers) {
  File file = LittleFS.open(filename, FILE_WRITE);  // Usamos FILE_WRITE para sobrescribir el archivo
  if (!file) {
    Serial.println("Error al abrir el archivo para guardar");
    return;
  }

  for (int i = 0; i < numUsers; i++) {
    if (file.write((uint8_t*)&credentials[i], sizeof(credentials[i])) != sizeof(credentials[i])) {
      Serial.println("Error al escribir los datos");
      file.close();
      return;
    }
  }
  
  Serial.println("Credenciales guardadas correctamente");
  file.close();
}

// Función para leer las credenciales del archivo
void readCredentials(const char* filename, user_t credentials[], int numUsers) {
  File file = LittleFS.open(filename, FILE_READ);
  if (!file) {
    Serial.println("Error al abrir el archivo para leer");
    return;
  }

  for (int i = 0; i < numUsers; i++) {
    if (file.read((uint8_t*)&credentials[i], sizeof(user_t)) != sizeof(user_t)) {
      Serial.println("Error al leer los datos");
      file.close();
      return;
    }
    Serial.println("Credenciales leídas correctamente");
  }

  file.close();
}

// Función para modificar una credencial en memoria
void modifyCredentials(user_t credentials[], int numUsers, const char* newName, const char* newPass, int index) {
  if (index >= 0 && index < numUsers) {
    strncpy(credentials[index].name, newName, sizeof(credentials[index].name) - 1);
    strncpy(credentials[index].pass, newPass, sizeof(credentials[index].pass) - 1);
    Serial.println("Credencial modificada correctamente");
  } else {
    Serial.println("Índice fuera de rango");
  }
}