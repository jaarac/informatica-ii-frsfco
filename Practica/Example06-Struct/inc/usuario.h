#ifndef USUARIO_H
#define USUARIO_H
#include <string.h>
/**
 * @brief Estructura que almacena nombre y clave
 * 
 */
typedef struct {
    char nombre[50];
    char clave[20];
} usuario_t;

/**
 * @brief Funcion que compara usuario y clave ingresada con la base de datos
 * 
 * @param usuarios Arreglo con los usuarios predefinidos cargados
 * @param numUsuarios Cantidad de usuarios pasados en el arreglo
 * @param nombre El nombre ingresado
 * @param clave La clave ingresada
 * @return int 1 = correcto, 0 = incorrecto
 */
int verificarClave(usuario_t usuarios[], int numUsuarios, const char *nombre, const char *clave);

#endif