#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include "../inc/usuario.h"

// Función para verificar si la clave ingresada coincide
int verificarClave(usuario_t usuarios[], int numUsuarios, const char *nombre, const char *clave) {
    for (int i = 0; i < numUsuarios; i++) {
        if (strcmp(usuarios[i].nombre, nombre) == 0 && strcmp(usuarios[i].clave, clave) == 0) {
            return 1; // Usuario y clave coinciden
        }
    }
    return 0; // No coincide
}