#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include "../inc/usuario.h"

#define MAX_USERS 5

void limpiarBuffer();

int main() {
    usuario_t usuarios[MAX_USERS] = {
        {"Fernando", "123"},
        {"Maria", "clave1"},
        {"Luis", "pass123"},
        {"Ana", "qwerty"},
        {"Carlos", "admin"}
    };

    char nombre[50];
    char clave[20];

    printf("Ingrese su nombre: ");
    if (fgets(nombre, sizeof(nombre), stdin)) {
        if (strchr(nombre, '\n') == NULL) { // No se encontró un salto de línea
            limpiarBuffer();               // Limpiar el búfer
        }
        nombre[strcspn(nombre, "\n")] = '\0'; // Eliminar el salto de línea si existe
    }

    printf("Ingrese su clave: ");
    if (fgets(clave, sizeof(clave), stdin)) {
        if (strchr(clave, '\n') == NULL) { // No se encontró un salto de línea
            limpiarBuffer();               // Limpiar el búfer
        }
        clave[strcspn(clave, "\n")] = '\0'; // Eliminar el salto de línea si existe
    }

    if (verificarClave(usuarios, MAX_USERS, nombre, clave)) {
        printf("Acceso concedido %s!\n",nombre);
    } else {
        printf("Acceso denegado.\n");
    }

    return 0;
}

// Función para limpiar el búfer de entrada
void limpiarBuffer() {
    int c;
    while ((c = getchar()) != '\n' && c != EOF); // Descarta caracteres restantes
}