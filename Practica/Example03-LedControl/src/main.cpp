#include <Arduino.h>

#define LED       13 //pin del LED
#define PULSADOR  6  //pin del pulsador

bool estado = true ;
bool estadoAnterior = true ;

void setup() {
  pinMode(PULSADOR, INPUT_PULLDOWN); //Conectar pulsador a +Vcc sin resistencia
                                    //Probar con INPUT e INPUT_PULLUP, tener en cuenta Resistencias
  pinMode(LED, OUTPUT);
}

void loop() {
  estado = digitalRead(PULSADOR);
    if (estado != estadoAnterior){      //hay cambio : Han pulsado o soltado
      if (estado == true){            //Al pulsar botón cambiar LED, pero no al soltar
            digitalWrite(LED, !digitalRead(LED)); //Invertimos el estado actual del LED
      }
      estadoAnterior = estado ;     // Para recordar el ultimo valor
    } 
}