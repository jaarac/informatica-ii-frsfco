#include <Arduino.h>
#include "../include/button.h"

#define T		10
#define PULSADOR    18

void setup() {
  inicializarBoton(PULSADOR);
}

void loop() {
  actualizarBoton(PULSADOR);
  delay(T);
}