#ifndef PULSADOR_H
#define PULSADOR_H

#include <Arduino.h>

typedef enum{
   ALTO,
   BAJO,
   DESCENDENTE,
   ASCENDENTE
} estadoBoton_t;

void errorBoton( void );
void inicializarBoton( int pulsador );
void actualizarBoton( int pulsador );
void botonPresionado( void );
void botonLiberado( void );

#endif