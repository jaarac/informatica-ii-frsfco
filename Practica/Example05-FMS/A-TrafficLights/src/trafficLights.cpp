#include "../include/led.h"
#include "../include/trafficLights.h"
#include <Arduino.h>

#define LED_VERDE 25
#define LED_AMARILLO 26
#define LED_ROJO 27

#define T_VERDE		300
#define T_AMARILLO	100
#define T_ROJO		500

void iniciarSemaforo()
{
    configurarLed(LED_ROJO, LED_AMARILLO, LED_VERDE);
}

void actualizarSemaforo()
{
    prenderLed('R');
    delay(T_ROJO);
    prenderLed('A');
    delay(T_AMARILLO);
    apagarLed('R');
    apagarLed('A');
    prenderLed('V');
    delay(T_VERDE);
    apagarLed('V');
    prenderLed('A');
    delay(T_AMARILLO);
    apagarLed('A');
}