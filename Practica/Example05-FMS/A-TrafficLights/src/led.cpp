#include "../include/led.h"
#include <Arduino.h>

static int ledRojo, ledAmarillo, ledVerde; //estas variables solamente son accesibles en este archivo

void configurarLed(int ledR, int ledA, int ledV)
{
    ledRojo = ledR;
    ledAmarillo = ledA;
    ledVerde = ledV;
    pinMode(ledRojo, OUTPUT);
    pinMode(ledAmarillo, OUTPUT);
    pinMode(ledVerde, OUTPUT);
}

void prenderLed(char led)
{
    switch (led)
    {
    case 'R':
    digitalWrite(ledRojo,1);
    break;
    case 'A':
    digitalWrite(ledAmarillo,1);
    break;
    case 'V':
    digitalWrite(ledVerde,1);
    break;
    default:
        break;
    }
}

void apagarLed(char led)
{
    switch (led)
    {
    case 'R':
        digitalWrite(ledRojo,0);
        break;
    case 'A':
        digitalWrite(ledAmarillo,0);
        break;
    case 'V':
        digitalWrite(ledVerde,0);
        break;
    default:
        break;
    }
}