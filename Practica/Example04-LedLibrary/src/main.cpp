/**
 * @file main.cpp
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2025-01-21
 * 
 * @copyright Copyright (c) 2025
 * 
 */
#include <Arduino.h>
#include <../include/led.h>

/**
 * @brief Tiempo de destello
 * 
 */
#define T 500//tiempo de destello

int pines [N_LED] = {12, 14};

void setup() {
  LedPinConfig(pines[0],pines[1]); //configuracion del pin del led
}

void loop() {
  // put your main code here, to run repeatedly:
  LedOn('R');
  delay(T);
  LedOff ('R');
  delay(T);
  LedOn('V');
  delay(T);
  LedOff ('V');
  delay(T);
}