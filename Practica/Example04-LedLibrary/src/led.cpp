/**
 * @file Led.cpp
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2025-01-21
 * 
 * @copyright Copyright (c) 2025
 * 
 */
#include "Arduino.h"
#include "Led.h"

/**
 * @brief Variable global "privada" al archivo. Solo puede accederse directamente dentro de este archivo.
 * 
 */
static int pinLed[2];

void LedPinConfig(int ledRojo, int ledVerde)
{
      pinLed[0] = ledRojo;
      pinLed[1] = ledVerde;
      for(int i=0; i< N_LED; i++){
            pinMode (pinLed[i], OUTPUT);
      }
}

void LedOn(char color)
{
      switch (color)
      {
      case 'R':
            digitalWrite (pinLed[0], 1);
            break;
      case 'V':
            digitalWrite (pinLed[1], 1);
            break;
      default:
            break;
      }
}

void LedOff(char color)
{
      switch (color)
      {
      case 'R':
            digitalWrite (pinLed[0], 0);
            break;
      case 'V':
            digitalWrite (pinLed[1], 0);
            break;
      default:
            break;
      }
}