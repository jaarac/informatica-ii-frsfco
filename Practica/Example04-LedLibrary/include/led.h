﻿/**
 * @file Led.h
 * @author L. Messi y B. Pitt
 * @brief Librería para uso de leds.
 * @version 0.1
 * @date 2025-01-21
 * 
 * @copyright Copyright (c) 2025
 * 
 */
#ifndef LED_h
#define LED_h

/**
 * @brief Cantidad de leds usados
 * 
 */
#define N_LED 2

/**
 * @brief Función para configurar el número de pin conectado al led.
 * 
 * @param ledRojo Pin del led rojo
 * @param ledVerde Pin del led verde
 */
void LedPinConfig(int ledRojo, int ledVerde);

/**
 * @brief Función para encender el led.
 * 
 * @param color 'R' para Rojo, 'V' para verde
 */
void LedOn(char color);

/**
 * @brief Función para apagar el led.
 * 
 * @param color 'R' para Rojo, 'V' para verde
 */
void LedOff(char color);
 
#endif
